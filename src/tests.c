#include <stdio.h>



#include "tests.h"


enum count_tests{
    AMOUNT_TESTS=5
};

void free_heap(void *heap) {
    munmap(heap, size_from_capacity((block_capacity) {.bytes = REGION_MIN_SIZE}).bytes);
}

int64_t success_allocate_memory(){
    printf("Test 1 started...\n");

    void *heap = heap_init(REGION_MIN_SIZE);
    debug_heap(stdout, heap);

    void *allocate = _malloc(1024);
    debug_heap(stdout, heap);

    if (!allocate) {
        printf("Test 1 failed(allocate problem)(\n");
        return 0;
    }

    _free(allocate);
    debug_heap(stdout, heap);
    free_heap(heap);

    printf("Test 1 success ended)\n");
    return 1;
}

int64_t free_block_from_some(){
    printf("Test 2 started...\n");
    void* heap = heap_init(REGION_MIN_SIZE);
    debug_heap(stdout, heap);

    void* allocate1 = _malloc(1024);
    void* allocate2 = _malloc(1024);


    debug_heap(stdout, heap);

    if (!heap || !allocate1 || !allocate2 ) {
        printf("Test 2 failed((\n");
        return 0;
    }

    _free(allocate2);

    debug_heap(stdout, heap);

    free_heap(heap);

    printf("Test 2 success ended))\n");
    return 1;
}

int64_t free_two_block_from_some(){
    printf("Test 3 started...\n");

    void *heap = heap_init(REGION_MIN_SIZE);
    debug_heap(stdout, heap);

    void *allocate1 = _malloc(1024);
    void *allocate2 = _malloc(1024);
    void *allocate3 = _malloc(1024);

    debug_heap(stdout, heap);

    if (!heap || !allocate1 || !allocate2 || !allocate3) {
        printf("Test 3 failed(((\n");
        return 0;
    }

    _free(allocate1);
    _free(allocate2);

    debug_heap(stdout, heap);

    free_heap(heap);

    printf("Test 3 success ended)))\n");
    return 1;
}

int64_t memory_is_over_new_region_expands_old(){
    printf("Test 4 started...\n");


    void *heap_little = heap_init(REGION_MIN_SIZE/2);
    debug_heap(stdout, heap_little);

    void *double_allocate = _malloc(REGION_MIN_SIZE * 2);
    debug_heap(stdout, heap_little);

    struct block_header *head = heap_little;
    if (head->capacity.bytes > REGION_MIN_SIZE) {
        _free(double_allocate);
        printf("Test 4 success ended))))\n");
        free_heap(heap_little);
        return 1;
    }else{
        _free(double_allocate);
        free_heap(heap_little);
        printf("Test 4 failed((((\n");
        return 0;
    }
}

int64_t memory_is_over_old_region_cant_expand(){
    printf("Test 5 started...\n");


    void *heap = heap_init(1024);
    debug_heap(stdout, heap);


    void *allocate_little = _malloc(512);
    debug_heap(stdout, heap);

    if (!heap || !allocate_little) {
        printf("Test 5 failed(((((\n");
        return 0;
    }

    struct block_header *head = (struct block_header*) (allocate_little - offsetof(struct block_header, contents));

    (void) mmap((void *) head->contents + head->capacity.bytes, REGION_MIN_SIZE, PROT_READ | PROT_WRITE,
                MAP_PRIVATE | MAP_FIXED, -1, 0);

    void *allocate2 = _malloc(1024);
    debug_heap(stdout, heap);

    if (!allocate2) {
        printf("Test 5 failed(((((\n");
        return 0;
    }

    _free(allocate_little);
    _free(allocate2);
    free_heap(heap);

    printf("Test 5 success ended)))))\n");
    return 1;
}

typedef int64_t test_functions();

test_functions* tests[]={
        success_allocate_memory,
        free_block_from_some,
        free_two_block_from_some,
        memory_is_over_new_region_expands_old,
        memory_is_over_old_region_cant_expand
};


int64_t run_tests(){
    printf("Testing started...\n");
    int64_t count_success_test=0;
    for(size_t i=0; i<AMOUNT_TESTS; i++){

        count_success_test+=tests[i]();

    }

    return count_success_test;
}
