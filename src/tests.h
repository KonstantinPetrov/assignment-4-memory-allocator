#ifndef LAB3_TESTS_H
#define LAB3_TESTS_H

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

#include <stdint.h>
#include <sys/mman.h>

int64_t run_tests();



#endif //LAB3_TESTS_H
